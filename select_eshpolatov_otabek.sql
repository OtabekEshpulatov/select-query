-- Task 2.


-- 1-solution to the 1st problem
SELECT DISTINCT ON (st.store_id)
    st.store_id,
    st.staff_id,
    SUM(pyt.amount) AS yearly_revenue
FROM
    staff st
JOIN
    payment pyt ON st.staff_id = pyt.staff_id
JOIN
    store str ON st.store_id = str.store_id
WHERE
    date_part('year', pyt.payment_date) = 2017
GROUP BY
    st.store_id, st.staff_id
ORDER BY
    st.store_id, yearly_revenue DESC;

-- 2-solution to the 1st problem
WITH yearly_revenue AS (
    SELECT
        st.store_id,
        st.staff_id,
        SUM(pyt.amount) AS yearly_revenue,
        RANK() OVER (PARTITION BY st.store_id ORDER BY SUM(pyt.amount) DESC) AS revenue_rank
    FROM
        staff st
    JOIN
        payment pyt ON st.staff_id = pyt.staff_id
    JOIN
        store str ON st.store_id = str.store_id
    WHERE
        date_part('year', pyt.payment_date) = 2017
    GROUP BY
        st.store_id, st.staff_id
)
SELECT
    store_id,
    staff_id,
    yearly_revenue
FROM
    yearly_revenue
WHERE
    revenue_rank = 1;

------------------------------------------------------------------------------------------------------------------------

-- Solution to the 2nd problem.
-- This query returns the most 5 rented movies. I could not figure out how can i display expected age for each movie because there
-- was not enough data, not birthdate of a customer who bought this movie. So i think you can take this into consideration
select count(*) as rental_count,
       inv.film_id
         from rental rn
         join inventory inv on inv.inventory_id = rn.inventory_id
         join film fm on fm.film_id = inv.film_id
group by inv.film_id
order by rental_count desc
limit 5;

------------------------------------------------------------------------------------------------------------------------

-- Solution to the 3rd problem.
WITH actor_interval AS (
    SELECT
        a.actor_id,
        CONCAT(a.first_name, ' ', a.last_name) AS actor_name,
        f.film_id,
        f.title,
        f.release_year AS release_year,
        LEAD(f.release_year) OVER (PARTITION BY a.actor_id ORDER BY f.release_year) AS next_release_year
    FROM
        public.actor a
        JOIN public.film_actor fa ON a.actor_id = fa.actor_id
        JOIN public.film f ON fa.film_id = f.film_id
)

SELECT
    ai.actor_id,
    ai.actor_name,
    ai.title                                                                AS from_film,
    LEAD(ai.title) OVER (PARTITION BY ai.actor_id ORDER BY ai.release_year) AS to_film,
    ai.release_year                                                         AS from_year,
    ai.next_release_year                                                    AS to_year,
    COALESCE(ai.next_release_year - ai.release_year, 0)                     AS interval_years
FROM
    actor_interval ai
ORDER BY
    interval_years DESC
LIMIT 1;


